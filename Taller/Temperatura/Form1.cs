﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Temperatura
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            double gfarenheit, ct1;
            ct1 = Convert.ToDouble(txtTemp.Text);
            gfarenheit = ct1 * 1.8 + 32.0;
            txtRes.Text = String.Format("{0:F3}", gfarenheit);
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            double gcentigrados, ct1;
            ct1 = Convert.ToDouble(txtTemp.Text);
            gcentigrados = (ct1 - 32.0) / 1.8;
            txtRes.Text = String.Format("{0:F3}", gcentigrados);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtTemp.Text = "";
            txtRes.Text = "";
            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
