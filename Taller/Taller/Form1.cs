﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Taller
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnMultiplicar_Click(object sender, EventArgs e)
        {
            double ct1, ct2;
            // extrae la entrada del usuario
            ct1 = Convert.ToDouble(txtOperando1.Text);
            ct2 = Convert.ToDouble(txtOperando2.Text);
            // muestra el resultado
            txtResultado.Text = String.Format("{0:F2}", ct1 * ct2);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtOperando1.Text = "";
            txtOperando2.Text = "";
            txtResultado.Text = "";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
