﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LlamarFormas
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        public string Nombre
        {
            set
            {
                label1.Text = value;
            }
        }
        public string Calle
        {
            get
            {
                return textBox1.Text;
            }
        }
        public string Colonia
        {
            get
            {
                return textBox2.Text;
            }
        }
        public string Delegación
        {
            get
            {
                return textBox3.Text;
            }
        }
        public string CódigoPostal
        {
            get
            {
                return textBox4.Text;
            }
        }
        public string Teléfono
        {
            get
            {
                return textBox5.Text;
            }
        }
    }
}
