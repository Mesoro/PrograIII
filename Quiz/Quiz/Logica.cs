﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    class Logica
    {

        public double Cantidad { get; set; }
        public String Edi { get; set; }
        public double Valor { get; set; }
        public double Costo { get; set; }
        public String Frase { get; set; }

        internal double Calcular (double num)
        {


            if (num < 12)
            {


                 
                Cantidad = 10;


            }

            else if (num>= 12 && num <= 65)
                
            {
                double a = 0;

                a = num - 12;
                Cantidad = (a * 2) + 10;

            }
            else
            {
                double b = 0;
                

                b = num - 65;
                Cantidad = (b * 4) + (54 * 2) + 10;
            
                
            }
            return Cantidad;
        }

        internal String Edificio(double edad)
        {

            if (edad >= 16 && edad <= 18)
            {
                Edi = "A";
            }
            else if (edad >=19 && edad <= 20)
            {
                Edi = "B";
            }
            else if (edad >= 21 && edad <= 25)
            {
                Edi = "C";
            }
            else
            {
                Edi = "NO ACEPTADO";
            }

            return Edi;
           
        }
        internal double Valores (double x, double d)
        {
            if (x <= 1 && d > 100)
            {
                Valor = d + (d * 0.20);
            }
            else if (1 < x && x <= 2 && d > 100)
            {
                Valor = d + (d * 0.30);
            }
            else if (2 < x && x <= 3 && d > 100)
            {
                Valor = d + (d * 0.40);
            }
            else if (x > 3 && d > 100)
            {
                Valor = d + (d * 0.50);
            }
            

            return Valor;
        }

        internal double Correo (double peso)
        {
            double num = 0;
            

            if (peso <= 20)

            {
                Costo = 10;
            }
            else if (peso > 20 && peso <= 30)
            {
                num = peso - 20;

                Costo = (num * 2) + 10;

            }
            else if (peso > 30 && peso <= 200)
            {
                num = peso - 30;
                Costo = (num * 1.5) + 40;
            }
            else if (peso > 200)
            {
            
            }


            return Costo;
        }

        internal String Algoritmo(double n1, double n2)
        {
            if (0 <= n1 && n1 <= n2 && n2<= 100)
            {
                Frase = "ESTA BIEN";
            }
            else
            {
                Frase = "NO ESTA BIEN";
            }



            return Frase;
                
        }
    }
}
