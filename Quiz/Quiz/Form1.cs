﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz
{
    public partial class Form1 : Form
    {
        Logica log = new Logica();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double num = Convert.ToDouble(textBox1.Text);

            double res = log.Calcular(num);

            
            
            textBox2.Text = String.Format("{0:F2}", res);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double num = Convert.ToDouble(textBox3.Text);
            String res = log.Edificio(num);

            textBox4.Text =  res;

        }

        private void button4_Click(object sender, EventArgs e)
        {

            double x = Convert.ToDouble(textBox5.Text);
            double d = Convert.ToDouble(textBox6.Text);

            double res = log.Valores(x, d);



            textBox7.Text = String.Format("{0:F2}", res);

        }

        private void button5_Click(object sender, EventArgs e)
        {

            double peso = Convert.ToDouble(textBox8.Text);
            

            double res = log.Correo(peso);



            textBox9.Text = String.Format("{0:F2}", res);


        }
    }
}
